import actors.createActor
import akka.actor._
import akka.actor.ActorDSL._
import akka.pattern.ask
import javas.DataFrame
import actors.suddenDeath


class Tomek(joe: ActorSelection) extends Actor {
  def receive = {
    case msg: String => println("Odebrałem cokolwiek");
    case df: DataFrame => println("przyszedł data frame z:  " + df.getNumberOfNeu() )
            df.manage();
            sender ! new suddenDeath()
    case _ => println("kaszanka");
  }
  override def preStart() {
    
   joe ! (new createActor(40.71, -74.00))
   Thread.sleep(100);
   joe ! (new createActor(40.71, -74.01))
   Thread.sleep(100);
   joe ! (new createActor(40.71, -74.02))
  }
    
}
object Greet_Sender extends App {

   println("STARTING")

   implicit val system = ActorSystem("GreetingSystem-1")

   val joe = system.actorSelection("akka.tcp://GreetingSystem@127.0.0.1:2552/user/joe")

   val a = system.actorOf(Props(new Tomek(joe)), "greeter");

}