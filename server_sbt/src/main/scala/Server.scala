
import java.util.Calendar

import actors.{Twitterek, createActor, getData}
import akka.actor.{ActorSystem, Props, Actor}
import twitter4j.GeoLocation

class Joe extends Actor {
//  var lista = List[Actor]()
  
  override def preStart(): Unit = {
     
  }
  def receive = {
//    case getData => val twitterManager = new TwitterManager();  
//                    sender ! twitterManager.performQuery(new GeoLocation(40.712784, -74.005941));  
    case createActor(x,y) =>  val now = Calendar.getInstance()
                              val currentMinute = now.get(Calendar.MINUTE)
                              val currentSec = now.get(Calendar.MILLISECOND);
                              println(currentMinute.toString() + currentSec.toString());
                              val child = context.actorOf(Props(new Twitterek(sender(),new GeoLocation(x,y))), name = "child" + currentMinute.toString() + currentSec.toString() )
                              println("Dostałem prośbę o danę i przesyłam sendera: " + sender())
                              child ! new getData()
    case _ => println("Received unknown msg ")
  }
}

object Server extends App {
  val system = ActorSystem("GreetingSystem")
  val joe = system.actorOf(Props[Joe], name = "joe")
//  println(joe.path)
  joe ! "local msg!"
  println("Server ready")
}
