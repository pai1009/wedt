package actors

sealed trait Message
 
case class getData() extends Message
 
case object stillAlive extends Message
 
case class suddenDeath() extends Message

case class createActor(x:Double, y:Double) extends Message 