package javas;

public class DataFrame implements java.io.Serializable{
	private int numberOfPos;
	private int numberOfNeu;
	private int numberOfNeg;
	private double x;
	private double y;
	public DataFrame(double xVal, double yVal) {
		this.numberOfNeg = 0;
		this.numberOfNeu = 0;
		this.numberOfPos = 0;
		this.x = xVal;
		this.y = yVal;
	}
	public void manage() {
//		ta funkcja wywolywana jest po odebraniu wyników klasyfikacji
//		wywolywana jest z Greet_sender, a sam df wypelniany jest w TwitterManager.java na Serverze
	}
	public int getNumberOfPos() {
		return numberOfPos;
	}
	public void setNumberOfPos(int numberOfPos) {
		this.numberOfPos = numberOfPos;
	}
	public int getNumberOfNeu() {
		return numberOfNeu;
	}
	public void setNumberOfNeu(int numberOfNeu) {
		this.numberOfNeu = numberOfNeu;
	}
	public int getNumberOfNeg() {
		return numberOfNeg; 
	}
	public void setNumberOfNeg(int numberOfNeg) {
		this.numberOfNeg = numberOfNeg;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
}
