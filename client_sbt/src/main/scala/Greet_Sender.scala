import javas.DataFrame

import actors.{createActor, suddenDeath}
import akka.actor._
import akka.event.Logging

import akka.io.IO

import spray.can.Http


class Tomek(joe: ActorSelection, x: Float, y: Float) extends Actor {
  def receive = {
    case msg: String => println("Odebrałem cokolwiek");
    case df: DataFrame => println("przyszedł data frame z:  " + df.getNumberOfNeu  )
            df.manage()
            sender ! new suddenDeath()
    case _ => println("");
  }
  override def preStart() {

    joe ! new createActor(x, y)
  }
}


object Greet_Sender extends App {

   println("STARTING")

   implicit val system = ActorSystem("GreetingSystem-1")

  /* Use Akka to create our Spray Service */
  val service = system.actorOf(Props[TodoWebServiceActor], "demo-service")

  /* and bind to Akka's I/O interface */
  IO(Http) ! Http.Bind(service, interface = "wedt-angular.pl", port = 8080)
}

trait RunSupervisorAgent {

  def test (x: Float, y: Float) = {
    Test("post zostal odebrany")
    implicit val system = ActorSystem("GreetingSystem-1")
    val joe = system.actorSelection("akka.tcp://GreetingSystem@127.0.0.1:2552/user/joe")

    val a = system.actorOf(Props(new Tomek(joe, x , y)), "greeter");

  }
}


/**
  * Actor to provide the Operations on TodoItems
  */
class SprayRestActor extends Actor with RunSupervisorAgent{
  val log = Logging(context.system, this)

  def receive = {
    //      case Get(id) => sender ! all();
    //      case Update(item) => sender ! update(item)
    //      case Delete(id) => sender ! delete(id)
    //      case Create(dueDate, text) => sender ! create(dueDate, text)
    case RunTomek(x,y) => sender ! test(x,y)
  }
}



