import akka.actor.{Actor, Props}
import org.json4s.DefaultFormats

import spray.httpx.Json4sSupport
import spray.routing._


/**
 * Actor to provide the routes of the rest services
 */
class TodoWebServiceActor extends Actor with TodoWebService {

  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(itemRoute ~ resourceRoute ~ webRoute ~ webResource2)
}

trait TodoWebService extends HttpService with PerRequestCreator with Json4sSupport {
  implicit def executionContext = actorRefFactory.dispatcher

  val json4sFormats = DefaultFormats

  // Takie widziałem w starym kodzie i jest przetestowane
 val resourceRoute = path("webapp" / "js" / "application.js") {
    get {
      getFromResource("webapp/js/application.js")
  }}

  // Takie widziałem i jeszcze nie testowałem
 val webRoute = path("webapp") {
    getFromResourceDirectory("webapp")
  }

  // katalog web jest w resources (czyli <projekt>/main/resources/web/[js/offers.js])
  //  uwaga, druga opcja nie zrobi Ci tak, że np localhost/main/ da localhost/main/index.html, trzeba
  //  wtedy dopisać kolejny path jak w tym pierwszym
 val webResource2 = path("index.html") { get {
    getFromResource("webapp/index.html")
  }}

  val itemRoute =
    pathPrefix("twitter") {
      path(LongNumber) { id: Long =>
        get {
          entity(as[Item]) {
            item =>
              handlePerRequest {
                RunTomek(item.x, item.y)
              }
          }
        }
      } ~ pathEnd {
        get {
          handlePerRequest {
            All
          }
        } ~ post {
          entity(as[Item]) {
            item =>
              handlePerRequest {
                RunTomek(item.x, item.y)
              }
          }
        }
      }}


  def handlePerRequest(message: RequestMessage): Route =
    ctx => perRequest(actorRefFactory, ctx, Props[SprayRestActor], message)
}



