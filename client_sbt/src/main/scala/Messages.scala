/**
 * Created by rguderlei on 25.02.14.
 */
sealed trait ResultMessage

case class Created(location: String) extends ResultMessage
case class Success(message: String) extends ResultMessage

case class Test(message: String) extends ResultMessage

case class Error(message: String)


sealed trait RequestMessage

/**
 * Message object for a request to retrieve form user the coordinates.
 */

case class RunTomek(x: Float, y:Float) extends RequestMessage


case object All extends RequestMessage
