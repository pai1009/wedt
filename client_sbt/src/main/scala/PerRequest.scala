import akka.actor._
import spray.http.StatusCodes._
import spray.http.{HttpHeader, StatusCode}
import akka.actor.SupervisorStrategy.Stop

import spray.routing.RequestContext
import akka.actor.OneForOneStrategy

import PerRequest.WithProps
import spray.httpx.Json4sSupport
import org.json4s.DefaultFormats


import scala.concurrent.duration._

trait PerRequest extends Actor with Json4sSupport{
    def r: RequestContext
    def target: ActorRef
    def message: RequestMessage

    import context._

    val json4sFormats = DefaultFormats

    setReceiveTimeout(7.seconds)

    target ! message

    def receive = {
    //  case Created(location) => complete(spray.http.StatusCodes.Created, "AAA", List(new Location(location)))
      case Test(message) => complete(spray.http.StatusCodes.Created, "run supervisor")
      case Error(message) => complete(BadRequest, message)
      case ReceiveTimeout => complete(GatewayTimeout, "Request timeout")
    }

    def complete[T <: AnyRef](status: StatusCode, obj: T, headers: List[HttpHeader] = List()) = {
      r.withHttpResponseHeadersMapped(oldheaders => oldheaders:::headers).complete(status, obj)
      stop(self)
    }

    override val supervisorStrategy =
      OneForOneStrategy() {
        case e => {
          complete(InternalServerError, Error(e.getMessage))
          Stop
        }
      }
}

object PerRequest {
  case class WithProps(r: RequestContext, props: Props, message: RequestMessage) extends PerRequest {
    lazy val target = context.actorOf(props)
  }
}

trait PerRequestCreator {
  def perRequest(actorRefFactory: ActorRefFactory, r: RequestContext, props: Props, message: RequestMessage) =
    actorRefFactory.actorOf(Props(new WithProps(r, props, message)))
}
