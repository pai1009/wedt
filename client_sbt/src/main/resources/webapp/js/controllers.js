var twitterControllers = angular.module('twitterControllers', []);

twitterControllers.controller('MessagesController', function($scope, $http) {
    $scope.greeting = [];

    $http.get('/helloall').success(function (data, status, headers, config) {
        $scope.greeting = data;
    }).error(function (data, status, headers, config) {
        $scope.errorMessage = "Can't retrieve messages list!";
    }); 
    
});
