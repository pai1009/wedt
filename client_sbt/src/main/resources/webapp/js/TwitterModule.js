/**
 * Created by pdybka on 27.05.16.
 */
var twitterModule = angular.module('twitterModule', []);

twitterModule.controller('RaportCreateController', ['$scope', '$http', '$location', 'notificationService',
    function(scope, http, location, notificationService) {
        scope.creatingRaport = null;

        scope.createRaport = function() {
            var coordinates = scope.creatingRaport;

            http.post("http://wedt-client.pl/twitter", coordinates, {
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                }
            }).success(function (data, status, headers, config) {
                //FIX: data.object.userName
                notificationService.success('Success', 'User ' + 'data.object.userName' + ' created successfully');
                location.path('/twitter');
            }).error(function (data, status, headers, config) {
                notificationService.error('Error', 'Creating user failed!');
                location.path('/twiiter');
            });
        }
    }
]);




