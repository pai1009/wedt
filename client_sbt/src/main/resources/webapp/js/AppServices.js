var appServices = angular.module('appServices', ['Postman']);

appServices.service('notificationService', ['postman', function(postman) {
    return {
        error: function(title, warning) {
            postman.error(title, warning)
        },
        info: function(title, warning) {
            postman.info(title, warning)
        },
        warning: function(title, warning) {
            postman.warn(title, warning)
        },
        success: function(title, warning) {
            postman.success(title, warning)
        }
    }
}]);

appServices.service('userService', ['$http', function(http) {
    return {
        getLoggedUser: function (callback) {
            http({method: 'GET', url: '/user/logged'}).then(function (result) {
                if(callback) {
                    callback(result.data.object);
                }
            });
        }
    };
}]);
