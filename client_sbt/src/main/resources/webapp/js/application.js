var twitter = angular.module('twitter', ['ngRoute', 'twitterModule', 'twitterControllers' , 'appServices']);


twitter.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/index', {
            templateUrl: 'index.html'
        })
        .when('/result', {
            templateUrl: 'templates/result.html'
        })
        .when('/twitter', {
            templateUrl: 'templates/create.html',
            controller: 'RaportCreateController'
        })
        .otherwise({
            redirectTo: '/twitter'
        });
}]);
