name := "client_sbt"

version := "1.0"

scalaVersion := "2.11.8"

// These options will be used for *all* versions.
scalacOptions ++= Seq(
  "-deprecation"
  ,"-unchecked"
  ,"-encoding", "UTF-8"
  ,"-target:jvm-1.7"
  // "-optimise"   // this option will slow your build
)

scalacOptions ++= Seq(
  "-Yclosure-elim",
  "-Yinline"
)

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

val akkaVersion = "2.3.6"
val sprayVersion = "1.3.1"



/* dependencies */
libraryDependencies ++= Seq (
  "org.twitter4j" % "twitter4j-core" % "4.0.4"
  ,"org.twitter4j" % "twitter4j-stream" % "4.0.4"
  ,"org.twitter4j" % "twitter4j-async" % "4.0.4"
  ,"org.twitter4j" % "twitter4j-media-support" % "4.0.4"
// -- Akka --
  ,"com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test"
  ,"com.typesafe.akka" %% "akka-actor" % akkaVersion
  ,"com.typesafe.akka" %% "akka-slf4j" % akkaVersion
  ,"com.typesafe.akka" %% "akka-remote" % akkaVersion
  ,"com.typesafe.akka" %% "akka-remote-tests" % akkaVersion
  // -- Spray --
  ,"io.spray" %% "spray-routing" % sprayVersion
  ,"io.spray" %% "spray-can" % sprayVersion
  ,"io.spray" %% "spray-httpx" % sprayVersion
  ,"io.spray" %% "spray-json" % sprayVersion
  ,"io.spray" %% "spray-testkit" % sprayVersion % "test"
  ,"org.json4s" %% "json4s-native" % "3.2.11"
  ,"com.typesafe.play" %% "play-json" % "2.4.0-M1"
  ,"com.h2database"      %   "h2"                % "1.3.174"
)

resolvers ++= Seq(
  "Twitter4J Repository" at "http://central.maven.org/maven2/",
  "sonatype releases"  at "http://oss.sonatype.org/content/repositories/releases/",
  "sonatype snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
  "typesafe repo"      at "http://repo.typesafe.com/typesafe/releases/",
  "spray repo"         at "http://repo.spray.io/"
)