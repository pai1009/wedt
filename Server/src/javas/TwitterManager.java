package javas;
import java.io.IOException;
import java.util.ArrayList;

import twitter4j.FilterQuery;
import twitter4j.GeoLocation;
import twitter4j.Place;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

 public class TwitterManager {  
    SentimentClassifier sentClassifier;  
    int LIMIT= 10; //the number of retrieved tweets  
    ConfigurationBuilder cb;  
    Twitter twitter;  
    public TwitterManager() {  
       cb = new ConfigurationBuilder();  
       cb.setOAuthConsumerKey("o0ziZX0cieXrOUbGwXEsUPaQz");  
       cb.setOAuthConsumerSecret("Y7bCmjCsvBIvXkJm1DWk3wb7T3RIpB6vC3qoCLrAzgxNusfs54");  
       cb.setOAuthAccessToken("723892649229488129-AF4AkEO0yYPMFVvCYa10Qk5k3a4EdhQ");  
       cb.setOAuthAccessTokenSecret("6cnZYq7tcViW76BZ6hUoTINnDuPRXJ6QkhddNsnQCX7KX");  
       twitter = new TwitterFactory(cb.build()).getInstance();  
       sentClassifier = new SentimentClassifier();  
    }  
    public DataFrame performQuery(GeoLocation geo) throws InterruptedException, IOException {  
//       Query query = new Query(inQuery);
    	DataFrame df = new DataFrame(geo.getLatitude(),geo.getLongitude());
    	
    	Query query = new Query();
    	query.geoCode(geo, 5, "mi");
       query.setCount(100);
       
       try {  
          int count=0;  
          QueryResult r;  
          do {  
             r = twitter.search(query);  
             ArrayList ts= (ArrayList) r.getTweets();  
             for (int i = 0; i < ts.size() && count < LIMIT; i++) {  
                count++;  
                Status t = (Status) ts.get(i);  
                String text = t.getText();  
                Place tweetLocation = t.getPlace();
//                System.out.println("Lokacja Tweeta: " + tweetLocation);
//                System.out.println("Text: " + text);  
                String name = t.getUser().getScreenName();
                String location = t.getUser().getLocation();
//                System.out.println("Location: " + location);  
                String sent = sentClassifier.classify(t.getText());  
                System.out.println("Sentiment: " + sent);
                if (sent.equals("neu")) {
                	df.setNumberOfNeu(df.getNumberOfNeu()+1);
                }
                if (sent.equals("neg")) {
                	df.setNumberOfNeg(df.getNumberOfNeg()+1);
                }
                if (sent.equals("pos")) {
                	df.setNumberOfPos(df.getNumberOfPos()+1);
                }
             }    
          } while ((query = r.nextQuery()) != null && count < LIMIT);  
       }  
       catch (TwitterException te) {  
          System.out.println("Couldn't connect: " + te);  
       }  
       return df;
    }  
 } 