package javas;
import java.io.File;
import java.io.IOException;

import com.aliasi.classify.ConditionalClassification;
import com.aliasi.classify.LMClassifier;
import com.aliasi.util.AbstractExternalizable;

public class SentimentClassifier {  
    String[] categories;  
    LMClassifier classa;  
    public SentimentClassifier() {  
    try {  
       classa= (LMClassifier) AbstractExternalizable.readObject(new File("classifier.txt"));  
       categories = classa.categories();  
    }  
    catch (ClassNotFoundException e) {  
       e.printStackTrace();  
    }  
    catch (IOException e) {  
       e.printStackTrace();  
    }  
    }  
    public String classify(String text) {  
    ConditionalClassification classification = classa.classify(text);  
    return classification.bestCategory();  
    }  
 }  