package actors

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import javas.TwitterManager
import twitter4j.GeoLocation




//case object Calculate

class Twitterek(human: ActorRef,geo: GeoLocation) extends Actor {
 
  override def preStart(): Unit = {
    
  }
  override def postStop(): Unit = {
    println("Umieram " + geo.getLatitude())
  }
 
  def receive = {
    case x: suddenDeath  => context.stop(self);               // samobójstwo
    case y: getData => val twitterManager = new TwitterManager();  
                    val frame = twitterManager.performQuery(geo);
                    println("Zebrałem dane i teraz wysyłam do " + human);
                    human ! frame;
     
  }
}